//
//  ViewController.swift
//  GeoARt
//
//  Created by Björn Wieczoreck on 09.06.18.
//  Copyright © 2018 Awesome Business. All rights reserved.
//

import UIKit
import ARKit
import Firebase
import FirebaseDatabase
import ARCore

enum SessionState: String {
    case idle, placeing, deleting
}

struct Model: Hashable {
    var anchorId: String
    var exploded: Double
    var rotation: Double
    var scale: Double
    var name: String
    var zoffset: Float
    
    init(snap: DataSnapshot) {
        self.anchorId = snap.childSnapshot(forPath: "anchorId").value as! String
        self.exploded = snap.childSnapshot(forPath: "exploded").value as! Double
        self.scale = snap.childSnapshot(forPath: "scale").value as! Double
        self.rotation = snap.childSnapshot(forPath: "rotation").value as! Double
        self.name = snap.childSnapshot(forPath: "name").value as! String
        self.zoffset = snap.childSnapshot(forPath: "zoffset").value as! Float
    }
    
    init(name: String) {
        self.name = name
        exploded = 0.0
        rotation = 0.0
        scale = 1.0
        anchorId = ""
        zoffset = 0.0
    }
}

class ViewController: UIViewController {
    
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet var propertySlider: UISlider!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var propertyLabel: UILabel!
    
    var sessionId: String!
    var sessionName: String!
    
    // model: node
    var models = [Model: SCNNode]()
    // anchorid: model
    var modelQueue = [String: Model]()
    var uploadingModel: Model? = nil
    
    var sessionState: SessionState = .idle {
        didSet {
            statusLabel.text = sessionState.rawValue
        }
    }
    
    var garSession: GARSession!
    
    var arAnchor: ARAnchor!
    var garAnchor: GARAnchor!
    var firebaseRef: DatabaseReference!
    
    required init?(coder aDecoder: NSCoder) {
        do {
            try garSession = GARSession(apiKey: "AIzaSyCfo4SEYs7nszwR6HJIGRduQ4e1pFCbtDE", bundleIdentifier: nil)
        } catch {
            print("error setting up GARSession \(error)")
        }
        super.init(coder: aDecoder)
        
        garSession.delegate = self
        garSession.delegateQueue = DispatchQueue.main
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        statusLabel.text = sessionState.rawValue
        
        sceneView.showsStatistics = true
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        sceneView.session.delegate = self
        sceneView.automaticallyUpdatesLighting = true
        sceneView.autoenablesDefaultLighting = true
        
        firebaseRef = Database.database().reference().child("sessions").child(sessionId)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)
        
        let delayInSeconds = 5.0
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
            
            self.firebaseRef.child("models").observe(.childAdded) { (snapshot) in
                
                // pls don't upload model while I do k thanks bye
                if self.uploadingModel != nil {
                    return
                }
                
                // skip the model if a model with this name is already there
                let modelName = snapshot.childSnapshot(forPath: "name").value as? String
                if modelName != nil {
                    if self.models.keys.contains(where: { (key) -> Bool in
                        key.name == modelName
                    }) {
                        return
                    }
                }
                
                let model = Model.init(snap: snapshot)
                if !self.modelQueue.keys.contains(model.anchorId) && !self.models.keys.contains(where: { (key) -> Bool in
                    key.name == model.name
                }) {
                    self.modelQueue.updateValue(model, forKey: model.anchorId)
                    do {
                        try self.garSession.resolveCloudAnchor(withIdentifier: model.anchorId)
                    } catch {
                        print("Error resolving cloud anchor \(error)")
                    }
                }
            }
            
            self.firebaseRef.child("models").observe(.childRemoved, with: { (snapshot) in
                let model = Model.init(snap: snapshot)
                let node = self.models[model]!
                self.models.removeValue(forKey: model)
                node.removeFromParentNode()
            })
            
            self.firebaseRef.child("models").observe(.childChanged, with: { (snapshot) in
                if snapshot.childrenCount != 6 {
                    return
                }
                let model = Model.init(snap: snapshot)
                for key in self.models.keys {
                    if key.name == model.name {
                        let node = self.models[key]!
                        node.rotation = SCNVector4(0, 1, 0, 0.01745329 * model.rotation)
                        node.scale = SCNVector3(model.scale, model.scale, model.scale)
                        // node.position = SCNVector3(node.position.x, node.position.y + model.zoffset, node.position.z)
                        self.models.removeValue(forKey: key)
                        self.models.updateValue(node, forKey: model)
                        break
                    }
                }
            })
            
            self.firebaseRef.child("models").observe(.childChanged, with: { (snapshot) in
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("Memory Warning")
    }
}

// MARK: - Placing models

extension ViewController {
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.count < 1 || sessionState == .idle {
            print("Ignoring touch event")
            return
        }
        
        let touch = touches.first!
        let touchLocation = touch.location(in: sceneView)
        
        if sessionState == .placeing {
            let hitTestResults = sceneView.hitTest(touchLocation, types: [ARHitTestResult.ResultType.existingPlane, ARHitTestResult.ResultType.existingPlaneUsingExtent, ARHitTestResult.ResultType.estimatedHorizontalPlane])

            if hitTestResults.count > 0 {
                let result = hitTestResults.first!
                addAnchorWithTransform(transform: result.worldTransform)
                let model = Model(name: "Brora_Sandstone_baked_1mil")
                uploadingModel = model
                addModelTransformed(transform: result.worldTransform, model: model)
            }
        }
    
        if sessionState == .deleting {
            let hitTestResults = sceneView.hitTest(touchLocation)
            guard let node = hitTestResults.first?.node else {
                return
            }
            
            node.removeFromParentNode()
        }
        
        sessionState = .idle
    }
    
    func addAnchorWithTransform(transform: matrix_float4x4) {
        arAnchor = ARAnchor(transform: transform)
        sceneView.session.add(anchor: arAnchor)
        do {
            try garAnchor = garSession.hostCloudAnchor(arAnchor)
            
        } catch {
            print("error :( \(error)")
        }
    }
    
    func addModel(x: Float = 0, y: Float = 0, z: Float = -0.2) {
        let box = SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0)

        let boxNode = SCNNode()
        boxNode.geometry = box
        boxNode.position = SCNVector3(x, y, z)
        
        sceneView.scene.rootNode.addChildNode(boxNode)
        
//        let geoScene = SCNScene(named: "art.scnassets/test.scn")!
//        let geoNode = geoScene.rootNode.childNode(withName: "raised_beach_3mil_", recursively: true)!
//        geoNode.position = SCNVector3(x, y, z)
//        sceneView.scene.rootNode.addChildNode(geoNode)
    }
    
    func addModelTransformed(transform: simd_float4x4, model: Model) {
//        let box = SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0)
//
//        let boxNode = SCNNode()
//        boxNode.geometry = box
//        boxNode.simdWorldTransform = transform
//        models.updateValue(boxNode, forKey: model)
//        sceneView.scene.rootNode.addChildNode(boxNode)
        
        let geoScene = SCNScene(named: "art.scnassets/Brora_Sandstone.scn")!
        let geoNode = geoScene.rootNode.childNode(withName: "Brora_Sndstne(1mill_ply)_Material.001", recursively: true)!
        geoNode.simdWorldTransform = transform
        geoNode.rotation = SCNVector4(0, 1, 0, 0.01745329 * model.rotation)
        geoNode.scale = SCNVector3(model.scale, model.scale, model.scale)
        // geoNode.position = SCNVector3(geoNode.position.x, geoNode.position.y + model.zoffset, geoNode.position.z)
        sceneView.scene.rootNode.addChildNode(geoNode)
        models.updateValue(geoNode, forKey: model)
    }
}

extension ViewController: ARSessionDelegate {
    
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        do {
            try garSession.update(frame)
        } catch {
            print("error updating frame")
        }
    }
}

// MARK: - GARSessionDelegate

extension ViewController: GARSessionDelegate {
    
    func session(_ session: GARSession, didHostAnchor anchor: GARAnchor) {
        print("did host anchor")
        guard var model = uploadingModel else {
            print("no uploadingModel available")
            return
        }
        let ref = firebaseRef.child("models").child(model.name);
        ref.child("name").setValue(model.name)
        ref.child("anchorId").setValue(anchor.cloudIdentifier)
        ref.child("scale").setValue(1)
        ref.child("exploded").setValue(0)
        ref.child("rotation").setValue(0)
        ref.child("zoffset").setValue(0)
        
        // update model entry in models for node
        let node = models[model]!
        models.removeValue(forKey: model)
        model.anchorId = anchor.cloudIdentifier!
        models.updateValue(node, forKey: model)
        uploadingModel = nil
    }
    
    func session(_ session: GARSession, didFailToHostAnchor anchor: GARAnchor) {
        print("did fail to host anchor")
        guard let model = uploadingModel else {
            print("no uploading model")
            return
        }
        let node = models[model]!
        models.removeValue(forKey: model)
        node.removeFromParentNode()
        uploadingModel = nil
    }
    
    func session(_ session: GARSession, didResolve anchor: GARAnchor) {
        guard let modelIdentifier = anchor.cloudIdentifier else {
            print("no cloud anchor to add model")
            return
        }
        print("resolved anchor")
        let model = modelQueue[modelIdentifier]!
        addModelTransformed(transform: anchor.transform, model: model)
        modelQueue.removeValue(forKey: modelIdentifier)
    }
    
    func session(_ session: GARSession, didFailToResolve anchor: GARAnchor) {
        guard let modelIdentifier = anchor.cloudIdentifier else {
            print("no cloud anchor to remove model")
            return
        }
        modelQueue.removeValue(forKey: modelIdentifier)
        print("did fail to resolve anchor")
    }
}

// MARK: - IBActions

extension ViewController {
    
    @IBAction func placingModel() {
        sessionState = sessionState != .placeing ? .placeing : .idle
    }
    
    @IBAction func deletingModel() {
        sessionState = sessionState != .deleting ? .deleting : .idle
    }
}

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}

//                let translation = result.worldTransform.translation
//                addModel(x: translation.x, y: translation.y, z: translation.z)
