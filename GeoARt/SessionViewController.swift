//
//  SessionViewController.swift
//  GeoARt
//
//  Created by Björn Wieczoreck on 09.06.18.
//  Copyright © 2018 Awesome Business. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

class SessionViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var firebaseRef: DatabaseReference!
    var sessions = [(String, String)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firebaseRef = Database.database().reference()
        
        firebaseRef.child("sessions").observe(.value) { (snapshot) in
            self.sessions.removeAll()
            
            snapshot.children.forEach({ (snap) in
                let session = snap as! DataSnapshot
                self.sessions.append((session.key, session.childSnapshot(forPath: "name").value as! String))
            })
            self.tableView.reloadData()
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        
        let button = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addSession))
        self.navigationItem.rightBarButtonItem = button
        self.navigationItem.title = "GeoARt"
    }
    
    @objc func addSession() {
        let alert = UIAlertController(title: "Create a new session", message: "Enter a name and create a new AR session!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Create session", style: .default, handler: { (action) in
            let myTextField = alert.textFields![0] as UITextField
            self.firebaseRef.child("sessions").childByAutoId().child("name").setValue(myTextField.text)
        }))
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "Enter session name"
        })
        self.present(alert, animated: true, completion: nil)
    }
}

extension SessionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let arViewController = storyboard.instantiateViewController(withIdentifier: "ArViewController") as! ViewController
        arViewController.sessionId = sessions[indexPath.row].0
        arViewController.sessionName = sessions[indexPath.row].1
        navigationController?.pushViewController(arViewController, animated: true)
    }
}

extension SessionViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sessions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "sessionCell")
        cell.textLabel!.text = sessions[indexPath.row].1
        cell.detailTextLabel!.text = sessions[indexPath.row].0
        return cell
    }
}
